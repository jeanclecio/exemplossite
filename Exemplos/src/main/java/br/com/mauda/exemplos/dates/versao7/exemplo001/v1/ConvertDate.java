package br.com.mauda.exemplos.dates.versao7.exemplo001.v1;

import java.util.Date;
import java.util.GregorianCalendar;

public class ConvertDate {

	public static Date parseDate(String date, String formato) {
		int primeiraBarra = date.indexOf("/");
		int segundaBarra = date.lastIndexOf("/");

		int dia = Integer.parseInt(date.substring(0, primeiraBarra));
		int mes = Integer.parseInt(date.substring(primeiraBarra + 1,
				segundaBarra));
		int ano = Integer.parseInt(date.substring(segundaBarra + 1));

		GregorianCalendar gc = new GregorianCalendar(ano, mes, dia);
		return gc.getTime();
	}
}