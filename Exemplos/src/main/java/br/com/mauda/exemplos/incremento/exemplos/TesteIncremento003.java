package br.com.mauda.exemplos.incremento.exemplos;

import org.junit.Test;

public class TesteIncremento003 { 
	
	@Test
	public void teste(){
		int varA = 0;
		int varB = 0;
		varB = ++varA + varB; 
		System.out.println("Valor varA = " + varA);
		System.out.println("Valor varB = " + varB);
	}
}