package br.com.mauda.puzzle.foreach.numero005;

public class Linguagem {
	
	private String nome;
	
	public Linguagem(String nome){
		this.nome=nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
