package br.com.mauda.puzzle.foreach.numero005;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
 
public class Teste { 
	
	private List<Linguagem> linguagens;
	
	@Before
	public void carregar(){
		linguagens = new ArrayList<Linguagem>();
		linguagens.add(new Linguagem("Java"));
		linguagens.add(new Linguagem("Scala"));
		linguagens.add(new Linguagem("Groovy"));
	}
	
	@Test
	public void testar(){
		Assert.assertFalse(linguagens.isEmpty());
		Assert.assertTrue(linguagens.size() == 3);
		
		for(Linguagem linguagem: linguagens){
			linguagem = new Linguagem("Clojure");
			System.out.println(linguagem);
		}
		
		for(Linguagem linguagem: linguagens){
			System.out.println(linguagem);
		}
	}
}