package br.com.mauda.puzzle.foreach.numero001;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
 
public class Teste { 
	
	private CollectionCustomizada<String> linguagens;
	
	@Before
	public void carregar(){
		linguagens = new CollectionCustomizada<String>();
		linguagens.add("Java");
		linguagens.add("Scala");
		linguagens.add("Groovy");
	}
	
	@Test
	public void teste(){
		Assert.assertFalse(linguagens.isEmpty());
		Assert.assertTrue(linguagens.size() == 3);
		
		for(String linguagem: linguagens){
			System.out.println(linguagem); 
		}
	}
}