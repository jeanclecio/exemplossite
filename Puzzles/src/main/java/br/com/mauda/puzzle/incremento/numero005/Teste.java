package br.com.mauda.puzzle.incremento.numero005;

import org.junit.Test;

public class Teste { 
	
	@Test
	public void teste(){
		ClasseInterna instancia = new ClasseInterna(5);
		System.out.println(instancia.metodo(instancia));
	}
}

class ClasseInterna {

	int x;

	public ClasseInterna(int y) {
		this.x = y++;
	}

	int metodo(ClasseInterna instancia) {
		if (instancia.x == 5) {
			instancia.x += 6;
			metodo(instancia);
		}
		if (instancia.x == 6) {
			instancia.x += 9;
			metodo(instancia);
		}
		return ++instancia.x;
	}
}